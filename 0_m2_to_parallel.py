import subprocess

def source(input, output):
    cmd = [f"""grep ^S {input} | cut -c 3- > {output+'_source.txt'}"""]
    subprocess.check_output(cmd, shell=True)

# Apply the edits of a single annotator to generate the corrected sentences.
def reference(input, output):
	m2 = open(input).read().strip().split("\n\n")
	out = open(output+'_reference.txt', "w")
	# Do not apply edits with these error types
	skip = {"noop", "UNK", "Um"}
	
	for sent in m2:
		sent = sent.split("\n")
		cor_sent = sent[0].split()[1:] # Ignore "S "
		edits = sent[1:]
		offset = 0
		for edit in edits:
			edit = edit.split("|||")
			if edit[1] in skip: continue # Ignore certain edits
			coder = int(edit[-1])
			if coder != 0: continue # Ignore other coders
			span = edit[0].split()[1:] # Ignore "A "
			start = int(span[0])
			end = int(span[1])
			cor = edit[2].split()
			cor_sent[start+offset:end+offset] = cor
			offset = offset-(end-start)+len(cor)
		out.write(" ".join(cor_sent)+"\n")

def error_categories(input, output):
	m2 = open(input).read().strip().split("\n\n")
	out = open(output+'_categories.txt', "w")
	# Do not apply edits with these error types
	skip = {"noop", "UNK", "Um"}

	for sent in m2:
		sent = sent.split("\n")
		cor_sent = []
		edits = sent[1:]
		offset = 0
		for edit in edits:
			edit = edit.split("|||")
			'''if edit[1] in skip:
				cor_sent
				continue # Ignore certain edits'''
			coder = int(edit[-1])
			if coder != 0: continue # Ignore other coders
			span = edit[0].split()[1:] # Ignore "A "
			start = int(span[0])
			end = int(span[1])
			cor = edit[1].split()
			cor_sent[start+offset:end+offset] = cor
			offset = offset-(end-start)+len(cor)

		out.write(" ".join(cor_sent)+"\n")

def main(set):
    reference(set['input'], set['output'])
    error_categories(set['input'], set['output'])

if __name__ == "__main__":
    A1 = {'input': 'source/A1.m2', 'output': '0_parallel_source/A1'}
    A2 = {'input': 'source/A2.m2', 'output': '0_parallel_source/A2'}
    A3 = {'input': 'source/A3.m2', 'output': '0_parallel_source/A3'}
    A4 = {'input': 'source/A4.m2', 'output': '0_parallel_source/A4'}
    A5 = {'input': 'source/A5.m2', 'output': '0_parallel_source/A5'}
    A6 = {'input': 'source/A6.m2', 'output': '0_parallel_source/A6'}
    A7 = {'input': 'source/A7.m2', 'output': '0_parallel_source/A7'}
    A8 = {'input': 'source/A8.m2', 'output': '0_parallel_source/A8'}
    A9 = {'input': 'source/A9.m2', 'output': '0_parallel_source/A9'}
    A10 = {'input': 'source/A10.m2', 'output': '0_parallel_source/A10'}

    sets = [A1, A2, A3, A4, A5, A6, A7, A8, A9, A10]
    source(A1['input'], '0_parallel_source/test')

    for set in sets:
        main(set)