import pandas as pd

def get_lines(file):
    with open(file, 'r') as f:
        lines = f.readlines()
        f.close()
    for i, line in enumerate(lines):
        lines[i] = line[:-1]
    return lines

def delete_blank(df):
    for i in range(10):
        df = df[df[f'Expected sentence (Annotation {i+1})'] != '']
    return df

def save_csv(df, output):
    return df.to_csv(output, index=False)

def fill_csv(source, sets):
    lines_source = get_lines(source)
    lines_reference, lines_categories = [], []
    for set in sets:
        lines_reference.append(get_lines(set['reference']))
        lines_categories.append(get_lines(set['categories']))

    matrix = [[a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u] for a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u in zip(lines_source, lines_reference[0],
                                                                                                                                                                    lines_categories[0], 
                                                                                                                                                                    lines_reference[1], 
                                                                                                                                                                    lines_categories[1],
                                                                                                                                                                    lines_reference[2], 
                                                                                                                                                                    lines_categories[2],
                                                                                                                                                                    lines_reference[3], 
                                                                                                                                                                    lines_categories[3],
                                                                                                                                                                    lines_reference[4], 
                                                                                                                                                                    lines_categories[4],
                                                                                                                                                                    lines_reference[5],
                                                                                                                                                                    lines_categories[5], 
                                                                                                                                                                    lines_reference[6], 
                                                                                                                                                                    lines_categories[6],
                                                                                                                                                                    lines_reference[7],
                                                                                                                                                                    lines_categories[7],
                                                                                                                                                                    lines_reference[8],
                                                                                                                                                                    lines_categories[8],
                                                                                                                                                                    lines_reference[9],
                                                                                                                                                                    lines_categories[9])]
    df = pd.DataFrame(matrix, columns=['Original sentence', 
                                        'Expected sentence (Annotation 1)', 
                                        'Mistake categories (Annotation 1)',
                                        'Expected sentence (Annotation 2)', 
                                        'Mistake categories (Annotation 2)',
                                        'Expected sentence (Annotation 3)', 
                                        'Mistake categories (Annotation 3)',
                                        'Expected sentence (Annotation 4)', 
                                        'Mistake categories (Annotation 4)',
                                        'Expected sentence (Annotation 5)', 
                                        'Mistake categories (Annotation 5)',
                                        'Expected sentence (Annotation 6)', 
                                        'Mistake categories (Annotation 6)',
                                        'Expected sentence (Annotation 7)', 
                                        'Mistake categories (Annotation 7)',
                                        'Expected sentence (Annotation 8)', 
                                        'Mistake categories (Annotation 8)',
                                        'Expected sentence (Annotation 9)', 
                                        'Mistake categories (Annotation 9)',
                                        'Expected sentence (Annotation 10)', 
                                        'Mistake categories (Annotation 10)'])
    df = delete_blank(df)
    return df

def main(source, sets, output):
    df_test = fill_csv(source, sets)
    save_csv(df_test, output)

if __name__ == "__main__":
    source = '0_parallel_source/test_source.txt'
    A1 = {'reference': '0_parallel_source/A1_reference.txt', 'categories': '0_parallel_source/A1_categories.txt'}
    A2 = {'reference': '0_parallel_source/A2_reference.txt', 'categories': '0_parallel_source/A2_categories.txt'}
    A3 = {'reference': '0_parallel_source/A3_reference.txt', 'categories': '0_parallel_source/A3_categories.txt'}
    A4 = {'reference': '0_parallel_source/A4_reference.txt', 'categories': '0_parallel_source/A4_categories.txt'}
    A5 = {'reference': '0_parallel_source/A5_reference.txt', 'categories': '0_parallel_source/A5_categories.txt'}
    A6 = {'reference': '0_parallel_source/A6_reference.txt', 'categories': '0_parallel_source/A6_categories.txt'}
    A7 = {'reference': '0_parallel_source/A7_reference.txt', 'categories': '0_parallel_source/A7_categories.txt'}
    A8 = {'reference': '0_parallel_source/A8_reference.txt', 'categories': '0_parallel_source/A8_categories.txt'}
    A9 = {'reference': '0_parallel_source/A9_reference.txt', 'categories': '0_parallel_source/A9_categories.txt'}
    A10 = {'reference': '0_parallel_source/A10_reference.txt', 'categories': '0_parallel_source/A10_categories.txt'}
    output = '1_set/test.csv'

    sets = [A1, A2, A3, A4, A5, A6, A7, A8, A9, A10]
    main(source, sets, output)