import pandas as pd
from nltk.tokenize.treebank import TreebankWordDetokenizer

def rules(sentence):
  """
  This function detokenizes quotes and hyphens.
  Input: str
  Example: 'I told you : "Why do you think I'm a ten-year-old guy ?"
  """
  return sentence.replace(' ?', '?').replace(' !', '!').replace(' ,', ',').replace(' .', '.').replace(' :', ':').replace(' ;', ';')

def nltk_detokenization(sentence):
  """
  This functions detokenizes all ponctuation marks, including paranthesis.
  Input: bag of words. 
  Example: ["I", "'ve", "many", "dogs", "."]
  """
  return TreebankWordDetokenizer().detokenize(sentence)

def detokenize(df):
  for _, row in df.iterrows():
    row[0] = rules(row[0])
    original = nltk_detokenization(row[0].split())
    row[0] = original
    for i in range(df.shape[1]//2):
      row[2*i+1] = rules(row[2*i+1])
      original = nltk_detokenization(row[2*i+1].split())
      row[2*i+1] = original
  return df

def save_csv(df, output):
  return df.to_csv(output, index=False)

def main(test):
  df = pd.read_csv(test['input'])
  df = detokenize(df)
  save_csv(df, test['output'])

if __name__ == "__main__":
  test = {'input': '1_set/test.csv', 'output': '2_detokenized_set/test.csv'}
  main(test)