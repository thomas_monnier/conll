import pandas as pd
import json
import itertools
import importlib

lib_1 = importlib.import_module('1_merge', package=None)

# Baselines 

def get_baselines(df):
  index = []
  column_errors_categories_dict = []
  for iter, row in df.iterrows():
    if iter !=0:
      error_categories = []
      error_categories_by_annotators = []
      s = True
      for i in range(df.shape[1]//2):
        if row[2*i+2] != '' and s == True:
          #error_categories.append(row[2*i+2])
          error_categories += row[2*i+2].split()
          error_categories_by_annotators.append(row[2*i+2].split())
        else:
          s = False
      if s is True:
        error_categories = list(dict.fromkeys(error_categories))
        error_categories_dict = {}
        for error_category in error_categories:
          error_categories_dict[error_category] = 0
          for errors_by_annotator in error_categories_by_annotators:
            if error_category in errors_by_annotator:
              error_categories_dict[error_category] += 1
        if max(error_categories_dict.values())>=8:
          index.append(iter)
          column_errors_categories_dict.append(json.dumps(dict(sorted(error_categories_dict.items(), key=lambda x: x[1], reverse=True)), indent=4))
  df = df.loc[index]
  df[21] = column_errors_categories_dict
  return index, df

# Suggestions (Rephrase-Like)

def append_void(length, list_):
    """
    Appends Nones to list to get length of list equal to `length`.
    If list is too long raise AttributeError
    """
    diff_len = length - len(list_)
    if diff_len < 0:
        raise AttributeError('Length error list is too long.')
    return list_ + [''] * diff_len
    
def get_rephrase_like(df):
  index = []
  for iter, row in df.iterrows():
    s = []
    for i in range(df.shape[1]//2):
      if row[2*i+2] == '':
        s.append(True)
      else:
        s.append(False)
    if s.count(True) >= 2 and 0 < s.count(False) <=9:
      index.append(iter)
      index_false = [i for i,x in enumerate(s) if x == False]
      res = [row[0]]
      expected_rephrase, errors_rephrase = [], []
      for i in index_false:
        if row[2*i+1] not in expected_rephrase:
          expected_rephrase.append(row[2*i+1])
          errors_rephrase.append(row[2*i+2])
      res += [x for x in itertools.chain.from_iterable(itertools.zip_longest(expected_rephrase, errors_rephrase)) if x]
      df.iloc[iter] = append_void(21, res)
  return index, df.iloc[index]

# Well-Written Sentences

def get_well_written_sentences(df):
  df = df[(df['Mistake categories (Annotation 1)'] == '')
        & (df['Mistake categories (Annotation 2)'] == '')
        & (df['Mistake categories (Annotation 3)'] == '')
        & (df['Mistake categories (Annotation 4)'] == '')
        & (df['Mistake categories (Annotation 5)'] == '')
        & (df['Mistake categories (Annotation 6)'] == '')
        & (df['Mistake categories (Annotation 7)'] == '')
        & (df['Mistake categories (Annotation 8)'] == '')
        & (df['Mistake categories (Annotation 9)'] == '')
        & (df['Mistake categories (Annotation 10)'] == '')]
  return list(df.index), df.iloc[:, :2]


# Ambigous Cases

def get_ambigous_cases(df, **kwargs):
  for _, value in kwargs.items():
    index = list(df.index)
    new_index = [i for i in index if i not in value]
    df = df.loc[new_index]
  return df

# Main

def main(sets):
    df = pd.read_csv(sets["input"])
    df = df.fillna('')

    index_rephrase, df_rephrase = get_rephrase_like(df)
    index_well_written, df_well_written = get_well_written_sentences(df)
    index_baselines, df_baselines = get_baselines(df)
    df_ambigous = get_ambigous_cases(df, index_rephrase=index_rephrase, index_baselines=index_baselines, index_well_written=index_well_written)

    lib_1.save_csv(df_rephrase, sets["suggestions"])
    lib_1.save_csv(df_well_written, sets["well_written"])
    lib_1.save_csv(df_baselines, sets["baselines"])
    lib_1.save_csv(df_ambigous, sets["ambigous"])

if __name__ == "__main__":
    sets = {"input": "2_detokenized_set/test.csv", "baselines": "3_types/baselines.csv", "suggestions": "3_types/suggestions.csv", "well_written": "3_types/well_written.csv", "ambigous": "3_types/ambigous.csv"}
    main(sets)