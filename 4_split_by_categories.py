import pandas as pd
import json
import importlib 

lib_1 = importlib.import_module("1_merge", package = None)

def extract_category_baselines(df, category):
    index = []
    for iter, row in df.iterrows():
        if iter != 0:
            mistake_categories = json.loads(row[21])
            if category in mistake_categories.keys():
                if mistake_categories[category] >= 7:
                    index.append(iter)
    return df.iloc[index]

def extract_category_suggestions(df, category):
    index = []
    for iter, row in df.iterrows():
        i, s = 2, False
        while i <= 19 and s != True:
            if category in row[i]:
                s = True
            else:
                row[i] = ''
                row[i-1] = ''
            i += 2
        if s:
            index.append(iter)
    return df.iloc[index]

def extract_category_ambigous(df, category):
    index = []
    for iter, row in df.iterrows():
        if iter > 0:
            s = False
            for i in range(df.shape[1]//2):
                if category in row[2*(i+1)]:
                    s = True
                else:
                    row[2*(i+1)] = ''
                    row[2*(i+1)-1] = ''
            if s:
                index.append(iter)
    return df.iloc[index]

def extract_category(df, key, category):
    if key == 'baselines':
        return extract_category_baselines(df, category)
    elif key == 'suggestions':
        return extract_category_suggestions(df, category)
    elif key == 'ambigous':
        return extract_category_ambigous(df, category) 
    else:
        print('Key: No match found. Value must be equal to {baselines, suggestions, ambigous}')

def main(category, **kwargs):
  print('Category: ', category)
  for key, value in kwargs.items():
    print('   File: ', key)
    df = pd.read_csv(value)
    df = df.fillna('')
    df_category = extract_category(df, key, category)
    lib_1.save_csv(df_category, f"4_category/{key}/{category}.csv")
    print('   Done')
    print('   ###############')
  print('###############')

if __name__ == "__main__":
    categories = pd.read_csv("error_categories.csv").Category
    sets = {"baselines": "3_types/baselines.csv", "ambigous": "3_types/ambigous.csv", "suggestions": "3_types/suggestions.csv"}

    for category in categories:
        main(category, baselines = sets["baselines"], ambigous = sets['ambigous'], suggestions = sets['suggestions'])